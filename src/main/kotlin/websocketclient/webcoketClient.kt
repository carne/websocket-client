package websocketclient


import org.apache.tomcat.websocket.WsWebSocketContainer
import org.springframework.messaging.converter.MappingJackson2MessageConverter
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaders
import org.springframework.messaging.simp.stomp.StompSession
import org.springframework.messaging.simp.stomp.StompSessionHandler
import org.springframework.web.socket.client.standard.StandardWebSocketClient
import org.springframework.web.socket.messaging.WebSocketStompClient
import org.springframework.web.socket.sockjs.client.SockJsClient
import org.springframework.web.socket.sockjs.client.Transport
import org.springframework.web.socket.sockjs.client.WebSocketTransport
import org.springframework.web.socket.sockjs.frame.Jackson2SockJsMessageCodec
import java.lang.reflect.Type
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.logging.Logger
import java.util.logging.Logger.getLogger
import kotlin.system.exitProcess


fun main(args: Array<String>) {

    val notificationClient = NotificationWebsocketStompClient()
    while (true) {
        val readLine = readLine() ?: ""

        when {
            readLine == "connect clear" -> notificationClient.disconnectAll()
            readLine.startsWith("connect") -> {
                val list = readLine.split(" ")
                if (list.size > 1) {
                    val countSession = list[1].toInt()
                    notificationClient.connect(countSession)
                }
            }
            readLine == "send" -> notificationClient.sendAll(Notification(id = ""))
            readLine == "close" -> exitProcess(1)
        }
    }
}

class NotificationWebsocketStompClient() {
    private val sessionHandler: NotificationStompSessionHandler = NotificationStompSessionHandler();


    fun connect(countSession: Int) {
        for (i in 1..countSession) {
            val transports: MutableList<Transport> = ArrayList<Transport>(1)
            transports.add(WebSocketTransport(StandardWebSocketClient(WsWebSocketContainer())))
//    transports.add(RestTemplateXhrTransport())
            val sockJsClient = SockJsClient(transports)
            sockJsClient.messageCodec = Jackson2SockJsMessageCodec()
            val stompClient = WebSocketStompClient(sockJsClient)

            stompClient.messageConverter = MappingJackson2MessageConverter()
            stompClient.connect("ws://localhost:8080/notification", NotificationStompSessionHandler())
        }
    }

    fun disconnectAll() {
        sessionHandler.disconnectAll()
    }

    fun sendAll(notification: Notification) {
        sessionHandler.sendAll(notification)
    }
}

data class Notification(val id: String)
class NotificationStompSessionHandler() : StompSessionHandler {
    private val log: Logger = getLogger("")
    override fun getPayloadType(headers: StompHeaders): Type {
        return Notification::class.java
    }

    override fun handleFrame(headers: StompHeaders, payload: Any?) {
        if (payload != null) {
            val message = payload as Notification
            log.info(message.toString())

        } else log.info("payload is null ")
    }

    override fun afterConnected(session: StompSession, connectedHeaders: StompHeaders) {
        session.subscribe("/notification", this)
        sessionHolder[session.sessionId] = session;
    }

    override fun handleException(session: StompSession, command: StompCommand?, headers: StompHeaders, payload: ByteArray, exception: Throwable) {
        exception.printStackTrace()

    }

    override fun handleTransportError(session: StompSession, exception: Throwable) {
        exception.printStackTrace()
    }

    fun sendAll(notification: Notification) {
        sessionHolder.forEach { (_, session) -> session.send("/accept", notification) }
        print("sendAll completing")
    }

    fun disconnectAll() {
        sessionHolder.forEach { (_, session) ->
            run {
                if (session.isConnected) {
                    session.disconnect()
                }
            }
        }
        sessionHolder.clear()
        print("disconnect completing")
    }

    companion object {
        val sessionHolder: ConcurrentMap<String, StompSession> = ConcurrentHashMap()
    }
}