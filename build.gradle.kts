plugins {
    java
    kotlin("jvm") version "1.4.30"
    application
}
//apply("java")
group = "den"
version = "1.0-SNAPSHOT"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.springframework:spring-websocket:5.2.2.RELEASE")
    implementation("org.springframework:spring-messaging:5.2.2.RELEASE")
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")


//    implementation("javax.websocket:javax.websocket-api:1.1")
    implementation("org.springframework.boot:spring-boot-starter-websocket:2.4.2")
    testCompile("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "websocektclient.websocketClient"
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "websocketclient.WebcoketClientKt"
    }
    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })

}